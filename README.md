**Requirements**


*  Composer
*  PHP 7.3
*  MySql
*  Apache

**How to Install**

* Take clone with *git clone https://gitlab.com/rahilpeerbits/demo-backend.git*
* cd demo-backend
* **Open terminal and type below command one by one**
*  composer install
*  cp .env.example .env
*  Now change your database credentials in .env file
*  php artisan config:cache
*  php artisan jwt:secret
*  php artisan migrate
*  php artisan key:generate
*  php artisan config:cache
*  php artisan serve

Now you will be able to check application through [http://127.0.0.1/app/](http://127.0.0.1/app/)
